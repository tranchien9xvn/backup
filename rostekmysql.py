# from configure import CONST
# from main import conn, cursor
import time, json, logging
import mysql.connector
from mysql.connector import Error
from mysql.connector import pooling
# from mtime import *
from configure import MYSQL_CONF

MAX_RETRY = 5

class PY_MYSQL:
	__instance = None
	__sqlErr = ""
	@staticmethod 
	def getInstance():
		if PY_MYSQL.__instance == None:
			PY_MYSQL()
		return PY_MYSQL.__instance

	def __init__(self):
		self.connection_pool = mysql.connector.pooling.MySQLConnectionPool(
			pool_name=MYSQL_CONF["pool_name"],
			pool_size=MYSQL_CONF["pool_size"],
			pool_reset_session=True,
			host=MYSQL_CONF["host"],
			port = MYSQL_CONF["port"],
			database=MYSQL_CONF["database"],
			user=MYSQL_CONF["user"],
			password=MYSQL_CONF["password"],
			auth_plugin='mysql_native_password'
		)
		# self.connection_pool = mysql.connector.pooling.MySQLConnectionPool(
		# 	pool_name="pynative_pool",
		# 	pool_size=10,
		# 	pool_reset_session=True,
		# 	host='127.0.0.1',
		# 	port = 3306,
		# 	database='rostekoee',
		# 	user='root',
		# 	password='rostek'
		# )

		if PY_MYSQL.__instance != None:
			raise Exception("Do not call __init__(). PY_MYSQL is a singleton!")
		else:
			PY_MYSQL.__instance = self

	@staticmethod
	def reloadConnectionPool():
		# self.lock.acquire()
		del PY_MYSQL.__instance
		PY_MYSQL.__instance = None
		logging.warning("AUTO RELOAD SQL CONNECTION POOL")
		PY_MYSQL()
		time.sleep(0.001)
		# self.lock.release()

	@staticmethod
	def getErrString():
		return PY_MYSQL.__sqlErr

	def fetchData(self,cmd):
		output = None
		PY_MYSQL.__sqlErr = "Sql fetch successfully"

		# isSuccess = False
		# for cnt in range(MAX_RETRY):
		# 	if isSuccess == True:
		# 		break
			
		# 	isSuccess = True
		##################SQL####################
		try:
			dbconnection = self.connection_pool.get_connection()
			if dbconnection.is_connected():
				c = dbconnection.cursor(dictionary=True)
				c.execute(cmd)
				output = c.fetchall()

		except Error as e :
			PY_MYSQL.__sqlErr = str(e)
			logging.error(PY_MYSQL.__sqlErr)
			if "pool exhausted" in PY_MYSQL.__sqlErr:
				# self.lock.acquire()
				PY_MYSQL.reloadConnectionPool()
				# self.lock.release()

		finally:
			#closing database connection.
			if(dbconnection.is_connected()):
				c.close()
				dbconnection.close()
				logging.info("MySQL connection is closed")
		
			return output

	def fetchOneData(self,cmd):
		output = None
		PY_MYSQL.__sqlErr = "Sql fetch successfully"
		# self.count = 0

		##################SQL####################
		try:
			dbconnection = self.connection_pool.get_connection()
			if dbconnection.is_connected():
				c = dbconnection.cursor(dictionary=True)
				c.execute(cmd)
				output = c.fetchone()    
			else:
				logging.info("MySQL connection is closed")
				PY_MYSQL.__sqlErr = "MySQL connection is closed"
		except Error as e :
			PY_MYSQL.__sqlErr = str(e)
			logging.error(PY_MYSQL.__sqlErr)
			if "pool exhausted" in PY_MYSQL.__sqlErr:
				# del PY_MYSQL.__instance
				# PY_MYSQL.__instance = None
				# logging.warning("AUTO RELOAD SQL CONNECTION POOL")
				# self.lock.acquire()
				PY_MYSQL.reloadConnectionPool()
				# self.lock.release()
		finally:
			#closing database connection.
			if(dbconnection.is_connected()):
				c.close()
				dbconnection.close()
				logging.info("MySQL connection is closed")

			return output
	
	def execute(self, cmd):
		resp = False
		PY_MYSQL.__sqlErr = "Sql execute unsuccessfully"
	
		##################SQL####################
		try:
			dbconnection = self.connection_pool.get_connection()
			if dbconnection.is_connected():
				c = dbconnection.cursor(dictionary=True)
				c.execute(cmd)   
				dbconnection.commit()
				PY_MYSQL.__sqlErr = "Sql execute successfully"
				resp = True

		except Error as e :
			resp = False
			PY_MYSQL.__sqlErr = str(e)
			logging.error(str(e))
			# logging.error ("Error while connecting to MySQL using Connection pool ", str(e))
			if "pool exhausted" in str(e):
				# del PY_MYSQL.__instance
				# PY_MYSQL.__instance = None
				# logging.warning("AUTO RELOAD SQL CONNECTION POOL")
				# self.lock.acquire()
				PY_MYSQL.reloadConnectionPool()
				# self.lock.release()
		finally:
			#closing database connection.
			if(dbconnection.is_connected()):
				c.close()
				dbconnection.close()
				logging.info("MySQL connection is closed")

			return resp

	def executeListCmd(self, listCmd):
		resp = True
		PY_MYSQL.__sqlErr = "Sql execute Successfully"
		try:
			dbconnection = self.connection_pool.get_connection()
			if dbconnection.is_connected():
				c = dbconnection.cursor(dictionary=True)
				c.execute("START TRANSACTION;")
				c.execute("SAVEPOINT SP1;")
				for cmd in listCmd:
					try:
						c.execute(cmd)
					except Exception as e:
						logging.error(e)	
						PY_MYSQL.__sqlErr = str(e)
						c.execute("ROLLBACK TO SP1;")
						print("Error: ")
						print(str(e))
						resp = False
						break
				dbconnection.commit()
			else:
				logging.error("MySQL connection is closed")
				resp = False
		except Error as e :
			resp = False
			PY_MYSQL.__sqlErr = str(e)
			logging.error(str(e))
		finally:
			#closing database connection.
			if(dbconnection.is_connected()):
				c.close()
				dbconnection.close()
				logging.info("MySQL connection is closed")
			return resp


	def createInsertListDataToTable(self, listData, tablename, othercmds):
		listCmd = []
		if othercmds:
			for othercmd in othercmds:
				listCmd.append(othercmd)
		for data in listData:
			listcolumn = ""
			listvalue  = ""
			for element in data:
				if data[element]:
					listcolumn += element + ","
					value = data[element]
					if (type(value) is str):
						if (element == 'date'):
							listvalue += "STR_TO_DATE('" + str(value) + "','%d-%m-%Y'),"
						else:
							listvalue += "'" + str(value) + "'," 
					else:
						listvalue += str(value)+ ","
			listvalue = listvalue[:-1]
			listcolumn = listcolumn[:-1]
			cmd = "INSERT INTO %s (%s) VALUES (%s); " % (tablename,listcolumn,listvalue)
			listCmd.append(cmd)
		
		return listCmd

	def insertListDataToTable(self, listData, tablename, othercmds):
		listCmd = self.createInsertListDataToTable(listData, tablename, othercmds)		
		return self.executeListCmd(listCmd)

	def createListCommand(self, listData, tablename):
		listCmd = []
		for data in listData:
			listcolumn = ""
			listvalue  = ""
			for element in data:
				if data[element]:
					listcolumn += element + ","
					value = data[element]
					if (type(value) is str):
						if (element == 'date'):
							listvalue += "STR_TO_DATE('" + str(value) + "','%d-%m-%Y'),"
						else:
							listvalue += "'" + str(value) + "'," 
					else:
						listvalue += str(value)+ ","
			listvalue = listvalue[:-1]
			listcolumn = listcolumn[:-1]
			cmd = "INSERT INTO %s (%s) VALUES (%s); " % (tablename,listcolumn,listvalue)
			listCmd.append(cmd)
		return listCmd
	
	def createListUpdateCommand(self, listData, tableName, col1, col2):
		listCmd = []
		for data in listData:
			if col1 in data and col2 in data:
				cmd = self.createUpdateCommand(tableName, data, col1, data[col1], col2, data[col2])
				if cmd:
					listCmd.append(cmd)
			else:
				logging("Update data missing {} or {}".format(col1, col2))
		return listCmd
	
	def createDeleteCommand2(self, tableName, col1, val1, col3, val3, col2, startVal, endVal):
		cmd = "DELETE FROM {} WHERE {}='{}' AND {}='{}' AND {} BETWEEN {} AND {}; ".format(tableName, col1, val1, col3, val3, col2, startVal, endVal)
		return cmd

	def fetchAll(self, tablename):
		cmd = "SELECT * FROM {};".format(tablename)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	def fetchInList(self, tablename, column, listValue):
		listValueStr = ''
		for value in listValue:
			val = '"' + value + '"' if type(value) == str  else str(value)
			listValueStr = (listValueStr + ',' + val ) if listValueStr != '' else val
		
		cmd = f"SELECT * FROM {tablename} WHERE {column} IN ({listValueStr});"
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	def fetchAllColumn(self, tablename, column):
		cmd = "SELECT {} FROM {};".format(column, tablename)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tablename WHERE column = value
	def fetchColumnWithCondition(self, tablename, retCol, column, value):
		cmd = "SELECT {} FROM {} WHERE {}='{}';".format(retCol, tablename, column, value)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tablename WHERE column = value
	def fetchAllWithCondition(self, tablename, column, value):
		cmd = "SELECT * FROM {} WHERE {}='{}';".format(tablename, column, value)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tablename WHERE column1 = value1 AND column2=value2;
	def fetchAllWithCondition2(self, tablename, column1, value1, column2, value2):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND {}='{}';".format(tablename, column1, value1, column2, value2)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tablename WHERE column1 = value1 AND column2=value2;
	def fetchAllWithCondition3(self, tablename, retCol, column1, value1, column2, value2):
		cmd = "SELECT {} FROM {} WHERE {}='{}' AND {}='{}';".format(retCol, tablename, column1, value1, column2, value2)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tablename WHERE column != value
	def fetchAllWithCondition4(self, tablename, column, value):
		cmd = "SELECT * FROM {} WHERE {}!='{}';".format(tablename, column, value)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tablename WHERE  column2 is not null;
	def fetchAllWithCondition5(self, tablename, column1):
		cmd = f"SELECT * FROM {tablename} WHERE {column1} IS NOT NULL;"
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	def fetchAllWithCondition6(self, tablename, column1, value1, column2, value2, column3,columnOrder):
		cmd = f"SELECT * FROM {tablename} WHERE {column1}={value1} AND {column2}<={value2} AND {column3} IS NULL ORDER BY {columnOrder};"
		# print(cmd)
		return self.fetchData(cmd)

	def fetchAllWithCondition7(self, tablename, column1, value1, column2, value2, column3,columnOrder):
		cmd = f"SELECT * FROM {tablename} WHERE {column1}={value1} AND {column2}>{value2} AND {column3} IS NULL ORDER BY {columnOrder} ASC;"
		# print(cmd)
		return self.fetchData(cmd)

	def fetchAllWithJoin(self, tablename1,tablename2,constraint1, constraint2):
		cmd = f"SELECT {tablename2}.id AS Die, {tablename2}.name AS Part, {tablename2}.weight AS MC_TON, {tablename2}.freq AS Frequecy, {tablename2}.position AS Position, {tablename2}.freq AS Frequecy, {tablename1}.name AS Jig_Name  ,{tablename1}.position AS Jig_Position  FROM {tablename2} LEFT JOIN {tablename1} ON {tablename1}.{constraint1}={tablename2}.{constraint2};"
		print(cmd)
		return self.fetchData(cmd)

	def fetchAllWithCondition8(self, tablename, column1, value1, column2, value2):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND ({} != '{}' OR {} IS NULL);".format(tablename, column1, value1, column2, value2,column2 )
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []
	# SELECT * FROM tablename WHERE column != value
	def fetchAllManyOrConds(self, tablename, column, *values):
		# cmd = "SELECT * FROM {} WHERE {}!='{}';".format(tablename, column, value)
		cmd = "SELECT * FROM {} WHERE "
		for val in values:
			cmd += "{} = '{}' OR ".format(column, val)

		# print(cmd)

		# data = self.fetchData(cmd)
		# if data:
		# 	return list(data)
		# else:
		# 	return []

	# SELECT TOP FROM tablename WHERE column1 = value1 AND column2=value2;
	def fetchOneWithCondition(self, tablename, column1, value1, column2, value2):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND {}='{}';".format(tablename, column1, value1, column2, value2)
		return self.fetchOneData(cmd)

	# SELECT TOP FROM tablename WHERE column = value
	def fetchOne(self, tablename, column, value):
		listObj = self.fetchAllWithCondition(tablename, column, value)
		if listObj:
			return listObj[0]
		else:
			return None

	# SELECT * FROM tableName WHERE columnName BETWEEN value1 AND value2;
	def fetchAllFromTo(self, tableName, columnName, value1, value2):
		cmd = "SELECT * FROM {} WHERE {} BETWEEN {} AND {};".format(tableName, columnName, value1, value2)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# SELECT * FROM tableName WHERE columnName1=value1 AND columnName2 BETWEEN value2 AND value3;
	def fetchAllConsAndFromTo(self, tableName, columnName1, value1, columnName2, value2, value3):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND {} BETWEEN {} AND {};".format(tableName, columnName1, value1, columnName2, value2, value3)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# def fetchLastestObject(self, tableName, col1, val1, colTime, date):
	# 	cmd = "SELECT * FROM {} WHERE ({}) IN ( SELECT MAX({}) AS {} FROM {} WHERE {}='{}' AND {} < {} );".format(tableName, colTime, colTime, colTime, tableName, col1, val1, colTime, date)
	# 	obj = self.fetchOneData(cmd)
	# 	print(obj)
	# 	return obj
	
	#Limit 10 days
	def fetchLastestObject(self, tableName, col1, val1, colTime, date):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND {} < {} AND {} > {} ORDER BY {} DESC;".format(tableName, col1, val1, colTime, date, colTime, date - 864000, colTime)
		obj = self.fetchOneData(cmd)
		return obj
	
	#Limit 10 days
	def fetchAllLaterObject(self, tableName, colTime, date):
		cmd = "SELECT * FROM {} WHERE {} < {} AND {} > {} ORDER BY {} DESC;".format(tableName, colTime, date, colTime, date - 864000, colTime)
		data = self.fetchData(cmd)
		if data:
			return list(data)
		else:
			return []

	# Find in tableName where col1=val1 and col2 != val2 WITH MIN(mincol)
	def fetchOneWithMinValue(self, tableName, minCol, col1, val1, col2, val2):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND {}!='{}' ORDER BY {};".format(tableName, col1, val1, col2, val2, minCol)
		return self.fetchOneData(cmd)

	# def fetchOneWithMinValue2(self, tableName, minCol, col1, val1, col2, val2, col3, val3):
	# 	cmd = "SELECT * FROM {} WHERE ({}) IN ( SELECT MIN({}) AS {} FROM {} WHERE {}='{}' AND {}!='{}' AND {}>{});".format(tableName, minCol, minCol, minCol, tableName, col1, val1, col2, val2, col3, val3)
	# 	print(cmd)
	# 	return self.fetchOneData(cmd)

	# Find in tableName where col1=val1 and col2 != val2 WITH MIN(mincol)
	def fetchOneWithMinValue2(self, tableName, minCol, col1, val1, col2, val2, col3, val3):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND {}!='{}' AND {}>={} ORDER BY {};".format(tableName, col1, val1, col2, val2, col3, val3, minCol)
		return self.fetchOneData(cmd)
	
	# Find in tableName where col1=val1 and col2 != val2 and col3!=val3 WITH MIN(mincol)
	def fetchOneWithMinValue3(self, tableName, minCol, col1, val1, col2, val2, col3, val3):
		cmd = "SELECT * FROM {} WHERE {}='{}' AND ({}='{}' OR {}='{}') ORDER BY {};".format(tableName, col1, val1, col2, val2, col3, val3, minCol)
		return self.fetchOneData(cmd)

	# find the second value order as increase
	def fetchTheSecondEnc(self, tableName, retcol, orderCol, condCol1, condVal1,condCol2,condVal2):
		cmd = "SELECT {} FROM {} WHERE {}='{}' AND {}!='{}' ORDER BY {}".format(retcol, tableName, condCol1, condVal1, condCol2, condVal2, orderCol)
		items = self.fetchData(cmd)
		if items and len(items) > 1:
			return items[1]
		else:
			return None

	def fetchMaxIdFrom(self, tableName):
		cmd = "SELECT MAX(id) AS id FROM {}".format(tableName)
		return self.fetchOneData(cmd)

	# find the second value order as increase
	def fetchTheSecondEnc2(self, tableName, retcol, orderCol, condCol1, condVal1,condCol2,condVal2, condCol3, condVal3):
		cmd = "SELECT {} FROM {} WHERE {}='{}' AND {}!='{}' AND {}>{} ORDER BY {}".format(retcol, tableName, condCol1, condVal1, condCol2, condVal2, condCol3, condVal3, orderCol)
		items = self.fetchData(cmd)
		if items and len(items) > 1:
			return items[1]
		else:
			return None

	# find the first value order as increase
	def fetchTheFirstInc(self, tableName, retcol, orderCol, condCol1, condVal1,condCol2,condVal2, condCol3, condVal3):
		cmd = "SELECT {} FROM {} WHERE {}='{}' AND {}!='{}' AND {}>{} ORDER BY {}".format(retcol, tableName, condCol1, condVal1, condCol2, condVal2, condCol3, condVal3, orderCol)
		return self.fetchOneData(cmd)

	def insertDataToTable(self, data, tablename):
		listcolumn = ""
		listvalue  = ""
		for element in data:
			listcolumn += element + ","
			value = data[element]
			if (type(value) is str):
				if (element == 'date'):
					listvalue += "STR_TO_DATE('" + str(value) + "','%d-%m-%Y'),"
				else:
					listvalue += "'" + str(value) + "'," 
			else:
				if value == None:
					listvalue += "NULL,"
				else:
					listvalue += str(value)+ ","
		listvalue = listvalue[:-1]
		listcolumn = listcolumn[:-1]
		cmd = "INSERT INTO %s (%s) VALUES (%s); " % (tablename,listcolumn,listvalue)
		return self.execute(cmd)
	
	def findValue(self,tablename, columnname, column, value):
		ID = 0
		if (type(value) is str):
			cmd = "SELECT %s FROM %s WHERE %s = '%s';" % (columnname, tablename,column,value)
		else:
			cmd = "SELECT %s FROM %s WHERE %s = %s;" % (columnname, tablename,column,value)
		rv = self.FetchData(cmd)
		if (len(rv)>0):
			ID = rv[0][columnname]
		return ID

	def findID(self, tablename, column, value):
		ID = 0
		if (type(value) is str):
			cmd = "SELECT id FROM %s WHERE %s = '%s';" % (tablename,column,value)
		else:
			cmd = "SELECT id FROM %s WHERE %s = %s;" % (tablename,column,value)
		rv = self.FetchData(cmd)
		if (len(rv)>0):
			ID = rv[0]["id"]
		return ID

	def createUpdateDataCmd(self, tablename, data, column, value):
		cmd = "UPDATE {} SET ".format(tablename)
		for element in data:
			if element == "id":
				continue

			if not data[element]:
				cmd += "{}={},".format(element, "NULL")
				continue

			if isinstance(data[element], str):
				cmd += "{}='{}',".format(element, data[element])
			else:
				cmd += "{}={},".format(element, data[element])
		cmd = cmd[:-1]
		cmd += " WHERE {}='{}';".format(column, value)
		return cmd

	# UPDATE tablename SET data WHERE column = value
	def updateData(self, tablename, data, column, value):
		cmd = self.createUpdateDataCmd(tablename, data, column, value)
		return self.execute(cmd)

	

	def updateData1(self, tablename, data, column, value):
		cmd = "UPDATE {} SET ".format(tablename)
		for element in data:
			if element == "id":
				continue

			if not data[element]:
				cmd += "{}={},".format(element, "NULL")
				continue

			if isinstance(data[element], str):
				cmd += "{}='{}',".format(element, data[element])
			else:
				cmd += "{}={},".format(element, data[element])
		cmd = cmd[:-1]
		cmd += " WHERE {}={};".format(column, value)
		return self.execute(cmd)

	def updateData2(self, tablename, data, column, value, colTime, timeFrom, timeTo):
		cmd = "UPDATE {} SET ".format(tablename)
		for element in data:
			if element == "id":
				continue

			if not data[element]:
				cmd += "{}={},".format(element, "NULL")
				continue

			if isinstance(data[element], str):
				cmd += "{}='{}',".format(element, data[element])
			else:
				cmd += "{}={},".format(element, data[element])
		cmd = cmd[:-1]
		cmd += " WHERE {}='{}' AND {} BETWEEN {} AND {};".format(column, value, colTime, timeFrom,timeTo)
		return self.execute(cmd)
	
	def createUpdateCommand(self, tablename, data, col1, val1, col2, val2):
		if not val1 or not val2:
			return None
		cmd = "UPDATE {} SET ".format(tablename)
		for element in data:
			if element == "id":
				continue

			if not data[element]:
				cmd += "{}={},".format(element, "NULL")
				continue

			if isinstance(data[element], str):
				cmd += "{}='{}',".format(element, data[element])
			else:
				cmd += "{}={},".format(element, data[element])
		cmd = cmd[:-1]
		cmd += " WHERE {}='{}' AND {}='{}';".format(col1, val1, col2, val2)
		return cmd
	
	def createDeleteCommand(self, tableName, column, value):
		cmd = "DELETE FROM {} WHERE {}='{}'".format(tableName, column, value)
		return cmd

	# DELETE * FROM tablename WHERE column = value
	def delete(self, tablename, column, value):
		cmd = "DELETE FROM {} WHERE {}='{}'".format(tablename, column, value)
		return self.execute(cmd)

	# DELETE * FROM tablename WHERE column = value
	def delete2(self, tablename, column, value, column2, value2):
		cmd = "DELETE FROM {} WHERE {}='{}' AND {}='{}'".format(tablename, column, value, column2, value2)
		return self.execute(cmd)