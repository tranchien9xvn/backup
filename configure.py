from pymongo import MongoClient
import arrow
import json
import os


## config flask
flask_host = '0.0.0.0'
flask_port = 5006
flask_debug = True
flask_autoReload = True
    
SYSCONF = {
    "work_start_time": 8,    # Start working time of day
    "timezone" : "Asia/Ho_Chi_Minh"
}

# Configure server address
SERVER_ADDRESS = '127.0.0.1'

# Configure Mysql Database
MYSQL_CONF = {
    "host" : SERVER_ADDRESS,
    "port" : 3306,
    "database": "nidec_mold",
    "user" : "root",
    "password" : "Rostek@2019",
    "pool_name" : "pynative_pool",
    "pool_size" : 30
}

# Configure MQTT Broker
MQTT_CONF = {
    "host" : SERVER_ADDRESS,
    "port" : 1883,
    "use_tls" : False,
    "time_live" : 60,
    "ca_certs" : "cert/server.cert",
}


#########################################
######### CHECK CONFIG UTIL #############
#########################################
class ConfigUtils:
    @staticmethod
    def checkOrderType(orderType):
        if orderType in OEE_CONF["order_type"]:
            return True
        else:
            return False


#########################################
######### STATUS MODEL      #############
#########################################

##configure password

sudo_password = ""

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or '12345678'

UPLOAD_FOLDER = os.getcwd() + '/upload/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



